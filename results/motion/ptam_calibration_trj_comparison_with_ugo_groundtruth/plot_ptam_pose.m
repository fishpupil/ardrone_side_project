%data=importdata('ptam_x_positive_only.txt',',',1); 
data=importdata('ptam_rectangle.txt',',',1); 


pose=struct('x',data.data(:,5),'y',data.data(:,6),'z',data.data(:,7));

pose.x = pose.x-pose.x(1);
pose.y = pose.y-pose.y(1);
pose.z = pose.z-pose.z(1);


figure();
subplot(311);
plot(pose.x);
legend('x');
xlabel('time', 'fontsize',16);
ylabel('metre', 'fontsize',16);

subplot(312);
plot(pose.y);
legend('y');
xlabel('time', 'fontsize',16);
ylabel('metre', 'fontsize',16);
subplot(313);
plot(pose.z);

legend('z');
xlabel('time', 'fontsize',16);
ylabel('metre', 'fontsize',16);

figure();

plot(pose.x,pose.z,'ro');



%{

 mean(z(180:230,1))
ans =
  -0.267410403250608
>> -0.45/ans
ans =
   1.682806631790894

%}

