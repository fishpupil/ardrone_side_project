sonar_z = importdata('sonar_z.txt');
ptam_z = importdata('ptam_z.txt');

%sonar_z = importdata('sonar_z_move_much.txt');
%ptam_z = importdata('ptam_z_move_much.txt');



sample_size=200;
figure;
plot(sonar_z,'b');
hold on;
plot(ptam_z,'r');
legend('sonar','ptam');

%{
%a=-sonar_z(54:54+sample_size);
a = -sonar_z;
b=ones(numel(a),1);
%c=ptam_z(54:54+sample_size);
c=ptam_z;

f = @(lamda)sum(    (a-(lamda(1)*b+lamda(2)*c)).^2          );
options = optimset('Display','Iter','MaxIter',2000,'MaxFunEvals',20000);
tic
[lamda,fval] = fminsearch(f,[1 1],options);
lamda
toc
%}

y = sonar_z;
x = ptam_z;