
figure();
begin_pt=1;
%%%%%%%%%%%%%%%%%%%
%  Plot traj
%%%%%%%%%%%%%%%%%%%
end_pt=numel(time_new);
interval=10;
x=new_t(:,1);
y=new_t(:,2);
z=new_t(:,3);
p1=plot3(x(begin_pt:interval:end_pt),y(begin_pt:interval:end_pt),z(begin_pt:interval:end_pt),'-ko');figure(gcf)
set(p1,'Color','black','LineWidth',2,'MarkerSize',8);
set(gca,'fontsize',16);

xlabel('x(metre)');
ylabel('y(metre)');
zlabel('z(metre)');
%axis([-1 1 -1 1]);

hold on;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Draw reference
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
goal_x = [0 0 1 1 0 ]';
goal_y = [0 -1 -1 0 0 ]';
goal_z = [0.8 0.8 0.8 0.8 0.8]';
p1=plot3(goal_x,goal_y,goal_z,'r');
set(p1,'Color','red','LineWidth',2);
%axis([-1.2 1.2 -1.2 1.2]);
%}
grid on;

view([-46 60])