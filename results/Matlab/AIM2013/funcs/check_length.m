len1=numel(time_new);
len2=numel(time);
if (len1-len2)==0
    disp('Same length');
else
    disp('Different length');
    if(len1 > len2)
        dif = len1-len2;
        for i=1:dif
            time(end+i)=time(end);
            t(end+1,:)=t(end,:);
            R(end+1,:)=R(end,:);
        end
    else
        dif = len2-len1;
        for i=1:dif
            time_new(end+i)=time_new(end);
            new_t(end+1,:)=new_t(end,:);
            new_R(end+1,:)=new_R(end,:);
        end
    end
end