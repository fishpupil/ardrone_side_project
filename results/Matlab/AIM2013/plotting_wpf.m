close all
clear all

path(path,'./funcs');

%================================
%   Load data
%================================

pose_new = importdata('./indoor_lab_wpf02/new_pose.txt',',',1);
pose = importdata('./indoor_lab_wpf02/pose.txt',',',1);
time_new = pose_new.data(:,1)/1e9;
time_new = time_new-time_new(1);
time = pose.data(:,1)/1e9;
time = time-time(1);
new_t = pose_new.data(:,5:7);
new_R = pose_new.data(:,8:10);
t = pose.data(:,5:7);
R = pose.data(:,8:10);
ts = mean(diff(time));

%================================
%   Plot data
%================================

plot_t;
plot_R;
plot_3D_wpf;
plot_3D_wpf_new;

%{
set(0,'CurrentFigure',fig);
fp = ginput(2);
start_t = fp(1,1);
end_t = fp(2,1);
b_idx=min(find(abs(time - start_t)< 0.2));
e_idx=min(find(abs(time - end_t)< 0.2));

std_t_new=std(new_t(b_idx:e_idx,:))
std_R_new=rad2deg(std(new_R(b_idx:e_idx,:)))
std_t=std(t(b_idx:e_idx,:))
std_R=rad2deg(std(R(b_idx:e_idx,:)))


fprintf('Time period = %fs ~ %fs\n',time(b_idx),time(e_idx));
fprintf('std x =%f(m)\n',std_t_new(1));
fprintf('std y =%f(m)\n',std_t_new(2));
fprintf('std z =%f(m)\n',std_t_new(3));
fprintf('std roll =%f(deg)\n',std_R_new(1));
fprintf('std pitch =%f(deg)\n',std_R_new(2));
fprintf('std yaw =%f(deg)\n',std_R_new(3));
fprintf('\n\n');
%}








