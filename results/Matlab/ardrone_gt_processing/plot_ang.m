figure;
subplot(311);
plot(time.vicon,gt_rpy(:,1));
hold on;
plot(time.nav,deg2rad(nav_ang(:,1)),'r');
legend('gt roll','nav roll');
subplot(312);
plot(time.vicon,gt_rpy(:,2));
hold on;
plot(time.nav,deg2rad(nav_ang(:,2)),'r');
legend('gt pitch','nav pitch');
subplot(313);
plot(time.vicon,gt_rpy(:,3));
hold on;
plot(time.nav,deg2rad(nav_ang(:,3)),'r');
legend('gt yaw','nav yaw');