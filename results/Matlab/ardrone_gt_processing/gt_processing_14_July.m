close all
clear all

nav = importdata('./data/ardrone/hover2.txt',',',1);
data=importdata('./data/vicon/hover2_vicon_gt.txt');  % import data

%nav = importdata('./data/ardrone/yaw3.txt',',',1);
%data=importdata('./data/vicon/yaw3_vicon_gt.txt');  % import data


ts=1/400; %40Hz
vel = struct('x',nav.data(:,10),'y',nav.data(:,11),'z',nav.data(:,12));

temp = nav.data(:,8);
temp = temp-temp(1);
angle = struct('roll',nav.data(:,6),'pitch',nav.data(:,7),'yaw',temp);

%======================================================
% Import all data
%======================================================
time=struct('server', data.data(:,1),'gspk', data.data(:,2),'nav',nav.data(:,1));

time.server = time.server - time.server(1);
time.gspk = time.gspk-time.gspk(1);
time.nav = (time.nav-time.nav(1))/1.0e+09;
time.interp = [0:ts:time.nav(end)]';

%time.interp = [0:ts:40]';

server_ts = mean(diff(time.server));



%figure();
%plot(t_interval_sys_time);

% These are VICON coordinate!!!
position = struct('x',  data.data(:,3)/1000, 'y',  data.data(:,4)/1000,'z', data.data(:,5)/1000);
norm = struct('x',  data.data(:,6), 'y',  data.data(:,7),'z', data.data(:,8));
orth = struct('x',  data.data(:,9), 'y',  data.data(:,10),'z', data.data(:,11));


% These are our coordinate!!!
% x forward, y right, z down (metre)

GT_pos = struct('x', position.z , 'y',  position.x,'z', -position.y);
GT_angle = struct('roll',  orth.y, 'pitch',  norm.z ,'yaw', orth.z);

GT_angle.roll = GT_angle.roll - GT_angle.roll(1);
GT_angle.pitch = GT_angle.pitch - GT_angle.pitch(1);
GT_angle.yaw = GT_angle.yaw - GT_angle.yaw(1);


GT_vel = struct('vel_x', diff(GT_pos.x)/server_ts , 'vel_y',  diff(GT_pos.y)/server_ts, 'vel_z', diff(GT_pos.z)/server_ts);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Angle interpolation (Vicon and ardrone) at 400Hz
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
temp1=interp1(time.server,GT_angle.roll,time.interp,'spline');
temp2=interp1(time.server,GT_angle.pitch,time.interp,'spline');
temp3=interp1(time.server,GT_angle.yaw,time.interp,'spline');

GT_angle_interp = struct('roll',  temp1, 'pitch',  temp2 ,'yaw', temp3);

temp1=interp1(time.nav,angle.roll,time.interp,'spline');
temp2=interp1(time.nav,angle.pitch,time.interp,'spline');
temp3=interp1(time.nav,angle.yaw,time.interp,'spline');

angle_interp = struct('roll',  temp1, 'pitch',  temp2 ,'yaw', temp3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Velocity interpolation (Vicon and ardrone) at 400Hz
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

temp1=interp1(time.server(1:end-1),GT_vel.vel_x,time.interp,'spline');
temp2=interp1(time.server(1:end-1),GT_vel.vel_y,time.interp,'spline');
temp3=interp1(time.server(1:end-1),GT_vel.vel_z,time.interp,'spline');

GT_vel_interp = struct('vel_x',  temp1, 'vel_y',  temp2 ,'vel_z', temp3);

temp1=interp1(time.nav,vel.x,time.interp,'spline');
temp2=interp1(time.nav,vel.y,time.interp,'spline');
temp3=interp1(time.nav,vel.z,time.interp,'spline');

vel_interp = struct('x',  temp1, 'y',  temp2 ,'z', temp3);

figure();
[x,lag]=xcorr(-GT_angle_interp.roll, angle_interp.roll);
plot(lag,x, 'o-');
[~,I_x]=max(x);
t_offset=lag(I_x);
fprintf('time lag=%f\n',t_offset);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       Median filter for all data
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%{

GT_vel_interp.vel_x = medfilt1(GT_vel_interp.vel_x, 0);
GT_vel_interp.vel_y = medfilt1(GT_vel_interp.vel_y, 100);
GT_vel_interp.vel_z = medfilt1(GT_vel_interp.vel_z, 100);

vel_interp.x = medfilt1(vel_interp.x, 40);
vel_interp.y = medfilt1(vel_interp.y, 40);
vel_interp.z = medfilt1(vel_interp.z, 40);

GT_angle_interp.roll = medfilt1(GT_angle_interp.roll, 20);
GT_angle_interp.pitch = medfilt1(GT_angle_interp.pitch, 20);
GT_angle_interp.yaw = medfilt1(GT_angle_interp.yaw, 20);
%}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%    Velocity plotting
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure()

subplot(211)
plot(time.interp+t_offset*ts, vel_interp.x,'r')
hold on
plot(time.interp,medfilt1(GT_vel_interp.vel_x,70),'b');

set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('m/s', 'fontsize',16);
h_legend=legend('ardrone vx', 'ground truth vx');
set(h_legend,'fontsize',16);
grid;

subplot(212)
plot(time.interp+t_offset*ts, vel_interp.y,'r')
hold on
plot(time.interp,medfilt1(GT_vel_interp.vel_y,70),'b');

set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('m/s', 'fontsize',16);
h_legend=legend('ardrone vy', 'ground truth vy');
set(h_legend,'fontsize',16);
grid;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%    Angle plotting
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure()

subplot(311)
plot(time.interp+t_offset*ts, angle_interp.roll,'r')
hold on
plot(time.interp,-radtodeg(GT_angle_interp.roll),'b');
set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('Deg', 'fontsize',16);
grid on;

legend('ardrone roll (degree)', 'ground truth roll')

subplot(312)
plot(time.interp+t_offset*ts, angle_interp.pitch, 'r')
hold on
plot(time.interp,radtodeg(GT_angle_interp.pitch),'b');
set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('Deg', 'fontsize',16);
grid on;
legend('ardrone pitch (degree)', 'ground truth pitch')

subplot(313)
plot(time.interp+t_offset*ts,angle_interp.yaw, 'r')
hold on
plot(time.interp,radtodeg(GT_angle_interp.yaw),'b');
set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('Deg', 'fontsize',16);
grid on;
legend('ardrone yaw (degree)' , 'ground truth yaw')


