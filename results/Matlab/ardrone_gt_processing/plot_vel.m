subplot(311)
plot(time.vicon(1:end-1),-gt_b_v(:,1),'b');
hold on;
plot(time.nav,nav_v(:,1),'r');
legend('gt x','nav x');
grid on;
xlabel('Time(s)');
ylabel('m/s');

subplot(312)
plot(time.vicon(1:end-1),gt_b_v(:,2),'b');
hold on;
plot(time.nav,nav_v(:,2),'r');
legend('gt y','nav y');
grid on;
xlabel('Time(s)');
ylabel('m/s');

subplot(313)
plot(time.vicon(1:end-1),gt_b_v(:,3),'b');
hold on;
plot(time.nav,nav_v(:,3),'r');
legend('gt z','nav z');
grid on;
xlabel('Time(s)');
ylabel('m/s');